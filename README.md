## RECIPE BOOK - Frontend

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.1.2.

Web Applications Teamwork Assignment for Telerik Academy Alpha and Fourth Bulgaria with JavaScript - Design and implement single-page web application using front-end framework of your choice.
The application will allow restaurant chefs to create and manage recipes composed of products with known nutrition values. During the interaction with the system users should see changes in nutrition information in real time. Newly created recipes could be tagged with a category from a list.

# Project Features

- x Authenticate users - Register, Login, Logout
- x Users can CRUD recipes
- x Recipes can contain as ingredients products from a list as well as other recipes.
- x Users can see the real-time changes in the nutrition values of the recipe being composed.
- x The nutrition values show the total energy in kcal, proteins, lipids and carbohydrates along with 18 other nutrients (vitamins, minerals, fatty acids, etc.)
- x When composing a new recipe users can search for products by foodgroup and/or by product description. The search results update real-time as the user types in the search field or upon selection of a specific foodgroup.
- x When composing a new recipe users can also search for other recipes by category and/or by title. The search results updates real-time as the user types in the search field or upon selection of a specific category.
- x The search results of ingredients show the ingredient's individual nutrition value per 100g.
- x In the main view recipes can be searched by title, category or specific range of nutrient values.

## Install dependencies

Run `npm install`
    `npm install -g @angular/cli`

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## Screenshots 

### Home

<a href="./screenshots/homescreen.png" target="_blank">
<img src="./screenshots/homescreen.png" width="280" alt="Image" />
</a>

### All Recipes

<a href="./screenshots/allrecipes.png" target="_blank">
<img src="./screenshots/allrecipes.png" width="280" alt="Image" />
</a>

### Single Recipe 

<a href="./screenshots/singlerecipe.png" target="_blank">
<img src="./screenshots/singlerecipe.png" width="280" alt="Image" />
</a>

<a href="./screenshots/singlerecipecard.png" target="_blank">
<img src="./screenshots/singlerecipecard.png" width="280" alt="Image" />
</a>

<a href="./screenshots/singlerecipeimg.png" target="_blank">
<img src="./screenshots/singlerecipeimg.png" width="280" alt="Image" />
</a>

### Create/Edit Recipe

<a href="./screenshots/editrecipe.png" target="_blank">
<img src="./screenshots/editrecipe.png" width="280" alt="Image" />
</a>

<a href="./screenshots/editrecipe2.png" target="_blank">
<img src="./screenshots/editrecipe2.png" width="280" alt="Image" />
</a>

<a href="./screenshots/editrecipe3.png" target="_blank">
<img src="./screenshots/editrecipe3.png" width="280" alt="Image" />
</a>

### Errors

<a href="./screenshots/notfound.png" target="_blank">
<img src="./screenshots/notfound.png" width="280" alt="Image" />
</a>

<a href="./screenshots/servererr.png" target="_blank">
<img src="./screenshots/servererr.png" width="280" alt="Image" />
</a>